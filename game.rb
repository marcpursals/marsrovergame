require_relative "rover"
require_relative "plateau"
require_relative "checkFile"

class Game

  def initialize(args)
    raise ArgumentError, "no arguments provided" unless args.length>0
    @args = args
    @file = getFile
    @check = args[1] && args[1]=="-check"
    @plateau = nil
    @rovers = nil
    @results = []
    @extraResults = []
    checkDataAndStartPlaying
  end

  def args
    @args
  end

  def plateau
    @plateau
  end

  def rovers
    @rovers
  end

  def results
    @results
  end

  def getFile
    begin
      File.readlines(@args[0])
    rescue
      raise StandardError, "We could not open file '#{@args[0]}'. Please be sure it exist and is placed on the same folder as playGame"
    end
  end

  def checkDataAndStartPlaying
    valued = CheckFile.new(@file)
    if valued
      setPlateau(valued.corners)
      setRovers(valued.rovers)
      play
    end
  end

  def setPlateau(corners)
    @plateau = Plateau.new(corners)
  end

  def setRovers(rovers)
    @rovers = rovers
  end

  def play
    rs = []
    @rovers.each do |rover|
      r = Rover.new(rover[0])
      position = r.getPosition(true)
      @plateau.parkRover(position)
      data = [r,rover[1].split("")]
      rs.push(data)
    end
    moveRovers(rs)
  end

  def moveRovers(rs)
    i=0
    rs.each do |rover,movement|
      begin
        i+=1
        movement.each_with_index do |move,index|
          actualPosition = rover.getPosition(true)
          nextPosition = rover.newMovement(move,true,true)
          moveBolean = (@check && move=="M") ? @plateau.freePosition?(nextPosition,true) && @plateau.insideGrid?(nextPosition) : @plateau.insideGrid?(nextPosition)
          if moveBolean
            @plateau.grid[actualPosition] = false
            rover.newMovement(move)
            @plateau.parkRover(nextPosition)
          else
            if @check
              message = "Rover #{i} was stoped because its future #{index+1}#{index==0 ? "st" : index==1 ? "nd" : index==2 ? "rd" : "th"} movement (position: #{nextPosition}) #{!@plateau.insideGrid?(nextPosition) ? "is outside boundaries" : "there is another rover parked on"}"
            else
              message = "Rover #{i} was stoped because its future #{index+1}#{index==0 ? "st" : index==1 ? "nd" : index==2 ? "rd" : "th"} movement (#{nextPosition}) is outside boundaries"
            end
            data = ["#{i}",message]
            # puts message
            @extraResults << data
            raise "Breaking movement loop"
          end
        end
      rescue
        next
      end
    end
    displayResults(rs)
  end

  def displayResults(rs)
    rs.each_with_index do |r,index|
      @results << r[0].getPosition
      extra = @extraResults.map do |x|
        x[1] if x[0].to_i==(index+1)
      end.compact
      puts "#{r[0].getPosition} #{extra[0] ? extra[0] : ""}"
    end
  end

end
