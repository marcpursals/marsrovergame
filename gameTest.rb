require "test/unit"
require_relative "game"

class GameTest < Test::Unit::TestCase

  def createTestFile(name,data)
    File.open(name, 'w') do |file|
      data.each do |d|
        file.puts d
      end
    end
  end

  def testInitialization
    assert_raise(ArgumentError){Game.new()}
    assert_raise(StandardError){Game.new(["fourier"])}
    createTestFile("testFile0.txt",["5 5"])
    game = Game.new(["testFile0.txt"])
    assert_equal(["testFile0.txt"],game.args)
    game2 = Game.new(["testFile0.txt","-check"])
    assert_equal(["testFile0.txt","-check"],game2.args)
    File.delete("testFile0.txt")
  end

  def testPlateauInitialization
    data = [
      "5 5",
      "1 2 N",
      "LMLMLMLMLMLM",
      "2 2 E",
      "LRMLLRMLR",
      "0 0 S",
      "LLLMMM"
    ]
    createTestFile("testFile2.txt",data)
    game = Game.new(["testFile2.txt"])
    assert_equal(36,game.plateau.grid.length)
    File.delete("testFile2.txt")
  end

  def testRoversInitialization
    data = [
      "5 5",
      "1 2 N",
      "LMLMLMLMLMLM",
      "2 2 E",
      "LRMLLRMLR",
      "0 0 S",
      "LLLMMM"
    ]
    createTestFile("testFile3.txt",data)
    game = Game.new(["testFile3.txt"])
    assert_equal(3,game.rovers.length)
    File.delete("testFile3.txt")
  end

  def testRoversInitializationError
    data = [
      "5 5",
      "1 2 N",
      "LMLMLMLMLMLM",
      "2 2 E",
      "LRMLLRMLRS",
      "0 0 S",
      "LLLMMM"
    ]
    createTestFile("testFile4.txt",data)
    assert_raise(StandardError){Game.new(["testFile4.txt"])}
    File.delete("testFile4.txt")
  end

  def testInitialParkOfRoversOnPLay
    data = [
      "2 2",
      "1 2 N",
      "LLLLLL",
      "2 2 E",
      "RRRRR",
      "0 0 S",
      "LLL"
    ]
    createTestFile("testFile5.txt",data)
    game = Game.new(["testFile5.txt"])
    assert_equal(true,game.plateau.grid["1 2"])
    assert_equal(true,game.plateau.grid["2 2"])
    assert_equal(true,game.plateau.grid["0 0"])
    File.delete("testFile5.txt")
  end

  def testExitCase
    data = [
      "2 2",
      "1 2 N",
      "LMLMLMLMLMLM",
      "2 2 E",
      "LRMLLRMLR",
      "0 0 S",
      "LLLMMM"
    ]
    createTestFile("testFile6.txt",data)
    game = Game.new(["testFile6.txt"])
    assert_equal(["0 1 S", "2 2 E", "0 0 W"],game.results)
    File.delete("testFile6.txt")
  end

  def testExitCaseWithCheck
    data = [
      "2 2",
      "1 2 N",
      "LMLMLMLMLMLM",
      "2 2 E",
      "LRMLLRMLR",
      "0 0 N",
      "LLLMMM",
      "2 0 N",
      "MMM"
    ]
    createTestFile("testFile7.txt",data)
    game = Game.new(["testFile7.txt","-check"])
    assert_equal(["0 1 S", "2 2 E", "1 0 E", "2 1 N"],game.results)
    File.delete("testFile7.txt")
  end

end
