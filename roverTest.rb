require_relative "rover"
require "test/unit"

class RoverTest < Test::Unit::TestCase

  def testInitialization
    rover = Rover.new("1 2 N")
    assert_equal("1 2 N", rover.getPosition)
  end

  def testInitializationRaise
    assert_raise(ArgumentError){Rover.new(2)}
    assert_raise(ArgumentError){Rover.new("1 2")}
  end

  def testMoveNorth
    rover = Rover.new("1 2 N")
    rover.move
    assert_equal("1 3 N", rover.getPosition)
  end

  def testMoveSouth
    rover = Rover.new("1 2 S")
    rover.move
    assert_equal("1 1 S", rover.getPosition)
  end

  def testMoveEast
    rover = Rover.new("1 2 E")
    rover.move
    assert_equal("2 2 E", rover.getPosition)
  end

  def testMoveWest
    rover = Rover.new("1 2 W")
    rover.move
    assert_equal("0 2 W", rover.getPosition)
  end

  def testMoveError
    rover = Rover.new("1 2 W")
    rover.move
    assert_equal("0 2 W", rover.getPosition)
  end

  def testRotateLeftFromNorth
    rover = Rover.new("1 2 N")
    rover.rotate("L")
    assert_not_equal("1 2 E", rover.getPosition)
  end

  def testRotateLeftFromSouth
    rover = Rover.new("1 2 S")
    rover.rotate("L")
    assert_equal("1 2 E", rover.getPosition)
  end

  def testRotateLeftFromEast
    rover = Rover.new("1 2 E")
    rover.rotate("L")
    assert_equal("1 2 N", rover.getPosition)
  end

  def testRotateLeftFromWest
    rover = Rover.new("1 2 W")
    rover.rotate("L")
    assert_equal("1 2 S", rover.getPosition)
  end

  def testRotateRightFromNorth
    rover = Rover.new("1 2 N")
    rover.rotate("R")
    assert_equal("1 2 E", rover.getPosition)
  end

  def testRotateRightFromSouth
    rover = Rover.new("1 2 S")
    rover.rotate("R")
    assert_equal("1 2 W", rover.getPosition)
  end

  def testRotateRightFromEast
    rover = Rover.new("1 2 E")
    rover.rotate("R")
    assert_equal("1 2 S", rover.getPosition)
  end

  def testRotateRightFromWest
    rover = Rover.new("1 2 W")
    rover.rotate("R")
    assert_equal("1 2 N", rover.getPosition)
  end

  def testRotateRightError
    rover = Rover.new("1 2 W")
    rover.rotate("R")
    assert_not_equal("1 2 S", rover.getPosition)
  end

  def testCheckNextPosition1
    rover = Rover.new("1 2 N")
    assert_equal("1 2 E", rover.newMovement("R",true))
    assert_equal("1 2 N", rover.getPosition)
  end

  def testCheckNextPosition2
    rover = Rover.new("1 2 N")
    assert_equal("1 2 W", rover.newMovement("L",true))
    assert_equal("1 2 N", rover.getPosition)
  end

  def testCheckNextPosition3
    rover = Rover.new("1 2 N")
    assert_equal("1 3", rover.newMovement("M",true,true))
    assert_equal("1 2 N", rover.getPosition)
  end

  def testCheckNextPositionError
    rover = Rover.new("1 2 N")
    assert_not_equal("1 3 E", rover.newMovement("M",true))
    assert_not_equal("1 2 E", rover.getPosition)
  end

  def testNewMovementLeft
    rover = Rover.new("1 2 N")
    assert_equal("1 2 W", rover.newMovement("L"))
    assert_equal("1 2 W", rover.getPosition)
  end

  def testNewMovementRight
    rover = Rover.new("1 2 N")
    assert_equal("1 2 E", rover.newMovement("R"))
    assert_equal("1 2 E", rover.getPosition)
  end

  def testNewMovementMove
    rover = Rover.new("1 2 N")
    assert_equal("1 3 N", rover.newMovement("M"))
    assert_equal("1 3 N", rover.getPosition)
  end

  def testNewMovementError
    rover = Rover.new("1 2 N")
    assert_not_equal("1 23 N", rover.newMovement("M"))
    assert_not_equal("1 23 N", rover.getPosition)
  end

end
