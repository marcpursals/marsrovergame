require "test/unit"
require_relative "checkFile"

class CheckFileTest < Test::Unit::TestCase

  def createFile(name,data)
    File.open(name, 'w') do |file|
      data.each do |d|
        file.puts d
      end
    end
    File.readlines(name)
  end

  def testInitializationCornerError1
    file = createFile("testCheckFile1.txt",["5 A","1 2 N","LMLMLMLMLMLM"])
    assert_raise(StandardError){CheckFile.new(file)}
    File.delete("testCheckFile1.txt")
  end

  def testInitializationCornerError1B
    file = createFile("testCheckFile1b.txt",["Testing fails","1 2 N","LMLMLMLMLMLM"])
    assert_raise(StandardError){CheckFile.new(file)}
    File.delete("testCheckFile1b.txt")
  end

  def testInitializationCornerSuccess
    file = createFile("testCheckFile2.txt",["5 5","1 2 N","LMLMLMLMLMLM"])
    cf = CheckFile.new(file)
    assert_equal("5 5",cf.corners)
    File.delete("testCheckFile2.txt")
  end

  def testInitializationRoversPositionError
    file = createFile("testCheckFile3.txt",["5 5","1 N","LMLMLMLMLMLM"])
    assert_raise(StandardError){CheckFile.new(file)}
    File.delete("testCheckFile3.txt")
  end

  def testInitializationRoversMovementError
    file = createFile("testCheckFile4.txt",["5 5","1 2 N","LMLMLMLMLMLMS"])
    assert_raise(StandardError){CheckFile.new(file)}
    File.delete("testCheckFile4.txt")
  end

  def testInitializationMultipleRoversMovementError
    data = [
      "5 5",
      "1 2 N",
      "LMLMLMLMLMLM",
      "2 2 E",
      "LRMLLRMLR",
      "0 0 S",
      "LLLMMMS"
    ]
    file = createFile("testCheckFile5.txt",data)
    assert_raise(StandardError){CheckFile.new(file)}
    File.delete("testCheckFile5.txt")
  end

  def testInitializationMultipleRoversPositionError
    data = [
      "5 5",
      "1 2 N",
      "LMLMLMLMLMLM",
      "2 2 E",
      "LRMLLRMLR",
      "0 S",
      "LLLMMM"
    ]
    file = createFile("testCheckFile6.txt",data)
    assert_raise(StandardError){CheckFile.new(file)}
    File.delete("testCheckFile6.txt")
  end

  def testInitializationMultipleRoversSuccess
    data = [
      "5 5",
      "1 2 N",
      "LMLMLMLMLMLM",
      "2 2 E",
      "LRMLLRMLR",
      "0 0 S",
      "LLLMMM"
    ]
    file = createFile("testCheckFile7.txt",data)
    cf = CheckFile.new(file)
    rovers = [
      ["1 2 N","LMLMLMLMLMLM"],
      ["2 2 E","LRMLLRMLR"],
      ["0 0 S","LLLMMM"]
    ]
    assert_equal("5 5",cf.corners)
    assert_equal(rovers,cf.rovers)
    File.delete("testCheckFile7.txt")
  end

  def testValidateNoRepeatedStartPosition
    data = [
      "5 5",
      "1 2 N",
      "LMLMLMLMLMLM",
      "1 2 N",
      "LRMLLRMLR",
      "0 0 S",
      "LLLMMM"
    ]
    file = createFile("testCheckFile8.txt",data)
    assert_raise(StandardError){CheckFile.new(file)}
    File.delete("testCheckFile8.txt")
  end

  def testValidateInitialPositionInsideBoudaries
    data = [
      "5 5",
      "1 2 N",
      "LMLMLMLMLMLM",
      "1 7 N",
      "LRMLLRMLR",
      "0 0 S",
      "LLLMMM"
    ]
    file = createFile("testCheckFile9.txt",data)
    assert_raise(StandardError){CheckFile.new(file)}
    File.delete("testCheckFile9.txt")
  end

  def testFileIsEmpty
    data = []
    file = createFile("testCheckFile10.txt",data)
    assert_raise(StandardError){CheckFile.new(file)}
    File.delete("testCheckFile10.txt")
  end

  def testValidateInitialPositionOutsideBoudaries
    data = [
      "20 25",
      "1 2 N",
      "LMLMLMLMLMLM",
      "1 7 N",
      "LRMLLRMLR",
      "22 0 S",
      "LLLMMM"
    ]
    file = createFile("testCheckFile10.txt",data)
    assert_raise(StandardError){CheckFile.new(file)}
    File.delete("testCheckFile10.txt")
  end

end
