class CheckFile
  def initialize(file)
    @file = file
    @positions = []
    @corners = checkCorner
    @rovers = checkRovers
  end

  def corners
    @corners
  end

  def rovers
    @rovers
  end

  def checkCorner
    if @file[0]
    ar = @file[0].split(" ")
    errorFound = ar.length != 2 ? true : numberOrNil(ar[0]) && numberOrNil(ar[1]) ? false : true
    raise StandardError, "File error. Data related to corner do not follow expected pattern 'X Y' where X & Y are integers" if errorFound
    @file[0].gsub("\n","")
    else
      raise StandardError, "File error. File is empty. Please fill the file"
    end
  end

  def checkRovers
    roversData = @file.drop(1)
    i = 0
    rovers = []
    roversData.each_slice(2) do |position,movement|
      i+=1
      position = position.gsub("\n","")
      movement = movement.gsub("\n","")
      if !validPosition?(position) || !validMovement?(movement)
        error = !validPosition?(position) ? ["position","1 2 N"] : ["movement","LMR"]
        raise StandardError, "File error. Data related to #{error[0]} do not follow pattern #{error[1]} on rover #{i}"
      end
      if @positions.include?(position)
        raise StandardError, "Repeated initial position for a rover founded on rover #{@positions.index(position)+1} and rover #{i}"
      end
      if outsideBoudaries?(position)
        raise StandardError, "Initial position of rover #{i} is outside of boundaries"
      end
      @positions.push(position)
      rovers.push([position,movement])
    end
    rovers
  end

  def validPosition?(position)
    position = position.split(" ")
    position.length==3 && numberOrNil(position[0]) && numberOrNil(position[1]) && numberOrNil(position[2]).nil?
  end

  def validMovement?(movement)
    return false if movement.nil?
    movement = movement.gsub("\n","").split("").uniq
    allowed = ["L", "R", "M"]
    diff = movement - allowed
    diff.length==0
  end

  def outsideBoudaries?(position)
    position = position.split(" ")
    corners = @corners.split(" ")
    corners[0].to_i<position[0].to_i || corners[1].to_i<position[1].to_i
  end

  def numberOrNil(string)
    Integer(string || '')
  rescue ArgumentError
    nil
  end
end
