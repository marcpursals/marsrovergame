class Rover

    def initialize(position)
      raise ArgumentError unless position.is_a?(String)
      position = position.split(" ")
      raise ArgumentError unless position.length == 3
      @x = position[0].to_i
      @y = position[1].to_i
      @orientation = position[2]
    end

    def move
      if @orientation=="N"
        @y+=1
      elsif @orientation=="S"
        @y-=1
      elsif @orientation=="E"
        @x+=1
      elsif @orientation=="W"
        @x-=1
      end
    end

    def rotate(rotation)
      return unless ["L","R"].include?(rotation)
      if @orientation=="N"
        @orientation = rotation=="L" ? "W" : "E"
      elsif @orientation=="S"
        @orientation = rotation=="L" ? "E" : "W"
      elsif @orientation=="E"
        @orientation = rotation=="L" ? "N" : "S"
      elsif @orientation=="W"
        @orientation = rotation=="L" ? "S" : "N"
      end
    end

    def resetContext(context)
      @x = context[0]
      @y = context[1]
      @orientation = context[2]
    end

    def getPosition(onlyPosition=false)
      onlyPosition ? @x.to_s+" "+@y.to_s : @x.to_s+" "+@y.to_s+" "+@orientation
    end

    def newMovement(movement,check=false,onlyPosition=false)
      return unless ["L","R","M"].include?(movement)
      initialContext = [@x, @y, @orientation]
      if movement == "R"
        rotate("R")
      elsif movement == "L"
        rotate("L")
      elsif movement == "M"
        move
      end
      newPosition = getPosition(onlyPosition)
      resetContext(initialContext) if check
      return newPosition
    end

  end
