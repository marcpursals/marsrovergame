class Help
  def initialize
  end

  def display
    puts "NAME"
    puts "    Mars Rover Game"
    puts "SYNOPSIS"
    puts "    ruby playGame.rb [filename] [-check]"
    puts "    or"
    puts "    ./playGame.rb [filename] [-check]"
    puts "DESCRIPTION"
    puts "    Mars Rover Game tries to determine the initial and final position of several rovers landed on a rectangular part of mars to whom we send movements instructions contained on a file"
    puts " "
    puts "    [filename]  File where game data is stored. Be sure it is a .txt file and it is placed at the same folder as playGame.rb"
    puts " "
    puts "    -check      Ideally, a flat rectangle with no collision possible could create fake final potitions for rovers."
    puts "                Use it if you want to double check if a rover is in antoher rover path and avoid collisions."
    puts "                This will affect to the final ouput of the game"
    puts ""
    puts "author: Marc Pursals"
    puts "date: 2/12/2017"
  end
end
