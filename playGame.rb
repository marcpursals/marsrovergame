#!/usr/bin/env ruby
require_relative "game"
require_relative "help"

if !ARGV.nil? && ARGV.length >0
  if ARGV[0]=="-h"
    Help.new().display
  else
    begin
      Game.new(ARGV)
    rescue ArgumentError => error
      puts error.message
    rescue StandardError => error
      puts error.message
    end
  end
elsif !ARGV.nil? && ARGV.length==0
  puts "Please use -h to find detailed run instructions"
end