class Plateau
  def initialize(border)
    raise ArgumentError unless border.is_a?(String)
    border = border.split(" ")
    raise ArgumentError unless border.length == 2
    @x = border[0].to_i
    @y = border[1].to_i
    @grid = {}
    resetGrid
  end

  def border
    @x.to_s+" "+@y.to_s
  end

  def grid
    @grid
  end

  def resetGrid
    (0...@x+1).to_a.each do |category|
      (0...@y+1).to_a.each do |value|
        key = category.to_s+" "+value.to_s
        @grid[key]=false
      end
    end
  end

  def checkPosition(position)
    raise ArgumentError unless position.is_a?(String)
    position = position.split(" ")
    raise ArgumentError unless position.length == 2
    position = position.join(" ")
    raise RangeError unless insideGrid?(position)
  end

  def insideGrid?(position)
    !@grid[position].nil?
  end

  def freePosition?(position,doNotCheck=false)
    checkPosition(position) unless doNotCheck
    !@grid[position]
  end

  def parkRover(position)
    checkPosition(position)
    return false unless freePosition?(position,true)
    @grid[position] = true
    return true
  end

end
