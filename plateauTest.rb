require "test/unit"
require_relative "plateau"

class PlateauTest < Test::Unit::TestCase

  @@grid = {}
  @@plateau = Plateau.new("2 2")

  def setDefaultGridValues
    @@grid["0 0"]= false
    @@grid["0 1"]= false
    @@grid["0 2"]= false
    @@grid["1 0"]= false
    @@grid["1 1"]= false
    @@grid["1 2"]= false
    @@grid["2 0"]= false
    @@grid["2 1"]= false
    @@grid["2 2"]= false
  end

  def testRaiseErrorOnInitialize
    assert_raise(ArgumentError){Plateau.new(23)}
    assert_raise(ArgumentError){Plateau.new("3")}
  end

  def testInitialization
    plateau = Plateau.new("5 5")
    assert_equal("5 5",plateau.border)
  end

  def testGridCreation
    setDefaultGridValues
    assert_equal(@@grid,@@plateau.grid)
  end

  def testGridKeyValue
    assert_equal(false,@@plateau.grid["0 0"])
  end

  def testFreePositionOnGrid
    assert_equal(true, @@plateau.freePosition?("0 0"))
    assert_raise(ArgumentError){@@plateau.parkRover(23)}
    assert_raise(ArgumentError){@@plateau.parkRover("2")}
    assert_raise(RangeError){@@plateau.parkRover("3 3")}
  end

  def testInsideGrid
    assert_equal(false, @@plateau.insideGrid?("3 3"))
    assert_equal(true, @@plateau.insideGrid?("1 1"))
  end

  def testParkRover
    assert_equal(true,@@plateau.parkRover("1 1"))
    assert_equal(false,@@plateau.parkRover("1 1"))
    @@grid["1 1"] = true
    assert_equal(@@grid,@@plateau.grid)
    assert_raise(ArgumentError){@@plateau.parkRover(23)}
    assert_raise(ArgumentError){@@plateau.parkRover("2")}
    assert_raise(RangeError){@@plateau.parkRover("3 3")}
    @@plateau.resetGrid
    setDefaultGridValues
  end

  def testParkMultipleRovers
    @@plateau.parkRover("1 1")
    @@plateau.parkRover("1 0")
    @@plateau.parkRover("2 2")
    @@grid["1 1"] = true
    @@grid["1 0"] = true
    @@grid["2 2"] = true
    assert_equal(@@grid,@@plateau.grid)
    @@plateau.resetGrid
    setDefaultGridValues
  end

end